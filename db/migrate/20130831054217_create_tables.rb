class CreateTables < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :email
      t.string :name
      t.timestamps
    end
    create_table :sketches do |t|
      t.references :user
      t.text :data
      t.timestamps
    end
    Sketch.create :user_id => 0, :data => '{"title": "blank", "time": 0,"color": "#336da5","bodies": [],"forces": [],"joints": [],"misc": [],"laws": {"gravity": false, "electric": false, "gravity_constant": 6.67384E-11, "electric_constant": 8.987E9, "near_earth_gravity": true, "earth_g": 9.807},"selected_item": null,"solver": {"type": "rk4", "delta_t": 0.10, "apply_constraints": true, "solve_collisions": true, "constraints_drift_correction": true, "use_velocity_constraint_model": true},"space_scale": 1,"center": {"x": 0.0, "y": 0.0},"frame_delay": 10,"scene": {"theme": {"colors": {"particle": "white", "disk": "yellow", "box": "red", "spring": "white", "linear_motor":"pink","circular_motor":"pink","anchor":"black","pin":"yellow", "plane": "yellow", "stick":"pink","tracer":"cyan","handle":"black"},"size":{"particle": 6, "disk": 50, "box": {"width": 60, "height": 40}, "linear_motor": 6, "circular_motor": 6, "handle": 4,"pin":6,"anchor":6,"tracer":6}}, "show_crosshair": true, "show_grid": false}}'
    Sketch.create :user_id => 0, :data => '{"title": "pendulum", "time": 0,"color": "#336da5","bodies": [{"type": "disk", "mass": 1.0, "position": {"x": 0, "y": 0}, "velocity": {"x": 0, "y": 0}, "inertia": 100.0, "acceleration": {"x": 0, "y": 0}, "angle": 0, "angular_velocity": 0, "angular_acceleration": 0, "name": "d1", "radius": 50, "height": 100, "charge": 0, "color": "orange"}],"forces": [],"joints": [{"name": "p1", "body": "d1", "color": "magenta", "local_position": {"x": 50, "y": 50}, "angle": 0, "type": "pin", "position": {"x": 50, "y": 50}}],"misc": [],"laws": {"gravity": false, "electric": false, "gravity_constant": 6.67384E-11, "electric_constant": 8.987E9, "near_earth_gravity": true, "earth_g": 9.807},"selected_item": null,"solver": {"type": "rk4", "delta_t": 0.10, "apply_constraints":true, "solve_collisions": true, "constraints_drift_correction": true, "use_velocity_constraint_model": true},"space_scale": 1,"center": {"x": 0.0, "y": 0.0},"frame_delay": 10,"scene": {"theme": {"colors": {"particle": "white", "disk": "yellow", "box": "red", "spring": "white", "linear_motor":"pink","circular_motor":"pink","anchor":"black","pin":"magenta", "plane": "yellow","stick":"pink","tracer":"cyan","handle":"black"},"size":{"particle": 6, "disk": 50, "box": {"width": 60, "height": 40}, "linear_motor": 6, "circular_motor": 6, "handle": 4,"pin":6,"anchor":6,"tracer":6}},"show_crosshair": true, "show_grid": false}}'
    Sketch.create :user_id => 0, :data => '{"title": "sun-earth", "time": 0,"color": "#336da5","bodies": [{"type": "particle", "mass": 1.989E30, "position": {"x": 0, "y": 0}, "velocity": {"x": 0, "y": 0}, "acceleration": {"x": 0, "y": 0}, "name": "Sun", "charge": 0, "color": "orange"},{"type": "particle", "mass": 5.972E24, "position": {"x": 1.5E11,"y": 0}, "velocity": {"x": 0,"y": 30000}, "acceleration": {"x": 0,"y": 0}, "name": "Earth", "charge": 0, "color": "blue"}],"forces": [],"joints": [],"misc": [{"name": "tracer1", "type": "tracer", "color": "white", "position": {"x": 1.5E11,"y": 0}, "body": "Earth", "local_position": {"x": 0, "y": 0},"points": []}],"laws": {"gravity": true, "electric": false, "gravity_constant": 6.67384E-11, "electric_constant": 8.987E9, "near_earth_gravity": false, "earth_g": 9.807},"selected_item": null,"solver": {"type": "rk4", "delta_t": 86400, "apply_constraints": false, "solve_collisions": true, "constraints_drift_correction": false, "use_velocity_constraint_model": true},"space_scale": 1.0E9,"center": {"x": 0.0, "y": 0.0},"frame_delay": 20,"scene": {"theme": {"colors": {"particle": "white", "disk": "yellow", "box": "red", "spring": "white", "linear_motor":"pink","circular_motor":"pink","anchor":"black","pin":"magenta", "plane": "yellow","stick":"pink","tracer":"cyan","handle":"black"},"size":{"particle": 6, "disk": 50, "box": {"width": 60, "height": 40}, "linear_motor": 6, "circular_motor": 6, "handle": 4,"pin":6,"anchor":6,"tracer":6}},"show_crosshair": true, "show_grid": false}}'
  end

  def down
    drop_table :sketches
    drop_table :users
  end
end
