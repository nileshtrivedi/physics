require 'rubygems'
require 'sinatra'
require 'rest-client'
require 'json'
require 'erb'
require 'sinatra/activerecord'
require './environments'

SITE_HOME = "http://localhost:4567"

class User < ActiveRecord::Base
  has_many :sketches
  
  def name
    "User id #{self.id}"
  end
  
  def sketch_array
    sketches.collect { |s| {:id => s.id, :title => s.title} }
  end
end

class Sketch < ActiveRecord::Base
  belongs_to :user
  
  def title
    "Sketch id #{self.id}"
  end
end

configure do
  #Sinatra::Application.reset!
  #use Rack::Reloader
end

# change session_secret for obvious reason
set :session_secret, '934cd52f3e7dae7ae2caebf4f6f0c937'
set :server, 'webrick'
enable :sessions


error 400..510 do
  'http error...'
end

helpers do
  def logged_in?
    @current_user
  end
end

before do
  uid = session[:uid]
  @current_user = User.find_by_id(uid) unless uid.blank?
end

get '/' do
  @sketch = Sketch.first
  erb :index
end

get '/sketch/:id' do
  @sketch = Sketch.find_by_id(params[:id]) || Sketch.first
  erb :index
end

post '/logout' do
  session[:uid] = nil
  session.clear
  #redirect '/'
  "logged out" # this will be an AJAX call. No need to redirect
end

post '/sketch/:id?' do
  content_type :json
  unless logged_in?
    return {:status => false, :message => "Not logged in"}.to_json
  end
  data = params[:data]
  if data.blank?
    return {:status => false, :message => "No data provided"}.to_json
  end
  if params[:id].blank?
    s = Sketch.new(:data => data)
    s.user = @current_user
  else
    s = Sketch.find_by_id(params[:id])
    if s.user_id == @current_user.id
      s.data = data #my sketch. Update
    else
      s = Sketch.new(:data => data)
      s.user = @current_user
    end
  end
  if s.save
    {:status => true, :id => s.id}.to_json
  else
    {:status => false, :id => s.id, :message => s.errors.first}.to_json
  end
end

post '/login' do
  content_type :json
  post_params = {
    :assertion => params["assertion"],
    :audience  => SITE_HOME
    #:audience  => "http://mechasim.herokuapp.com"
  }
  resp = RestClient::Resource.new("https://verifier.login.persona.org/verify",
                                  :verify_ssl => true
                                 ).post(post_params)
  data = JSON.parse(resp)
  if data["status"].eql?("okay")
    user = User.find_by_email(data["email"])
    if user
      puts "user found: #{user.inspect}"
    else
      puts "user not found for #{data['email']}. Creating one..."
      user = User.create({:email => data["email"]})
    end
    session[:uid] = user.id
    data[:logged_in] = true
    data[:uid] = user.id
    data[:email] = user.email
    data[:name] = user.name
    data[:sketches] = user.sketch_array
    puts "successful login of #{user.email}"
    data.to_json
  else
    puts "not sure about the data: #{data.inspect}"
    return {:status => "error"}.to_json
  end
end
