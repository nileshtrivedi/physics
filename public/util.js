ko.extenders.numeric = function(target, precision) {
    //adding a "numeric" extension to KnockOut
    //this is needed because browsers allow strings to be set as value of number input fields
    //we create a writeable computed observable to intercept writes to our observable
    //this way. we make sure that our model always contains numbers, once this extension is applied to an observable
    //we even for the field to change if invalid string was specified for a numeric field
    var result = ko.computed({
        read: target,  //always return the original observables value
        write: function(newValue) {
            var current = target(),
                fl = parseFloat(newValue),
                valueToWrite = isNaN(fl) ? 0.0 : fl;
 
            //only write if it changed
            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                //if the parsed value is the same, but a different value was written, force a notification for the current field
                if (newValue !== current) {
                    target.notifySubscribers(valueToWrite);
                }
            }
        }
    });
 
    //initialize with current value to make sure it is validated appropriately
    result(target());
 
    //return the new computed observable
    return result;
};

ko.bindingHandlers.numberInput = {
    init: function (element, valueAccessor, allBindingsAccessor) {
      var value = valueAccessor();
      element.value = value();
      element.onchange = function () {            
          var strValue = this.value;
          var numValue = Number(strValue);
          numValue = isNaN(numValue) ? 0 : numValue;
          this.value = numValue;
          value(numValue);
      };
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
      var value = valueAccessor();
      element.value = value();
    }
};


function getMousePos(canvas, evt) {
  // evt contains coordinates in the normal page coordinates
  // we need to return coords relative to the canvas element

  var rect = canvas.getBoundingClientRect();
  
  //the following is taken from the net. Commented out right now, but may be required later for browser resizing
	//getting mouse position correctly, being mindful of resizing that may have occured in the browser:
	//var rect = canvas.getBoundingClientRect();
	//return {
	//  x: (evt.clientX - rect.left) * (canvas.width/rect.width),
	//  y: (evt.clientY - rect.top) * (canvas.height/rect.height)
	//};
		
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
};


if (typeof String.prototype.startsWith != 'function') {
  // startsWith is not a standard JS function
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}

function simpleXhrSentinel(xhr) {
    return function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200){
                // reload page to reflect new login state
                window.location.reload();
              }
            else {
                navigator.id.logout();
                alert("XMLHttpRequest error: " + xhr.status); 
            } 
        } 
      } 
}

function verifyAssertion(assertion) {
    // Your backend must return HTTP status code 200 to indicate successful
    // verification of user's email address and it must arrange for the binding
    // of currentUser to said address when the page is reloaded
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/xhr/sign-in", true);
    // see http://www.openjs.com/articles/ajax_xmlhttp_using_post.php
    var param = "assert="+assertion;
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("Content-length", param.length);
    xhr.setRequestHeader("Connection", "close");
    xhr.send(param); // for verification by your backend

    xhr.onreadystatechange = simpleXhrSentinel(xhr);
}

function signoutUser() {
    // Your backend must return HTTP status code 200 to indicate successful
    // sign out (usually the resetting of one or more session variables) and
    // it must arrange for the binding of currentUser to 'null' when the page
    // is reloaded
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/xhr/sign-out", true);
    xhr.send(null);
    xhr.onreadystatechange = simpleXhrSentinel(xhr);
}

function updateUIAfterLogIn(data) {
  //console.log("updating UI after successful login...");
  window.user_model.logged_in(data["logged_in"]);
  window.user_model.uid(data["uid"]);
  window.user_model.email(data["email"]);
  window.user_model.name(data["name"]);
  window.user_model.sketches(data["sketches"]);
}

function resetUIAfterLogOut(data) {
  //console.log("reseting UI after successful logout...");
  window.user_model.logged_in(false);
  window.user_model.uid(undefined);
  window.user_model.email(undefined);
  window.user_model.name("anonymous");
  window.user_model.sketches([]);
}

function save_sketch() {
  $.ajax({
    type: 'POST',
    url: ('/sketch/' + (window.user_model.current_sketch_id() || "")),
    data: {
      data: ko.mapping.toJSON(world)
    },
    success: function(res, status, xhr) {
      if (res["status"] == true) {
        console.log("Sketch saved.");
        console.log(res);
      } else {
        console.log("Sketch save failed");
        console.log(res);
      }
    },
    error: function(res, status, xhr) {
      console.log("sketch save failed");
      console.log(res);
    }
  });
}

