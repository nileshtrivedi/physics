"use strict";
/* TODO
- Handles for angular velocity
- handles for motors
- move attachments when moving / updating an item
- Stick joints
- Show forces for the selected body
- Drag force
- Attaching joints correctly
- Better popups for adding constant/weight/custom forces
- Continuous Collision detection & response
- Webapp. lazy registration, saving & sharing the simulation
- Complete Notes, Meters, and Tracers
- Graphs, controllers
- Text description for the diagram
- simulation/full undo
- toolbar icons, logo etc, colors, textures/gradients etc.
- icon for custom force

- show units with all numbers
- Add text and image items (HTML?)
- attach camera to a body

- Backward simulation - just use negative time-step?
- optimize unnecessary calls of draw_world() (perhaps in world_changed())
- optimize draw_spring (use triangle wave instead of sinusoidal for shape)
- Make sure code for custom_force does not contain any dangerous JS
- select items by drawing a box? How to handle multiple selection?
- Polygons, GJK collision detection
- Ropes, chains, Pulleys
*/
var canvas;
var context;
var is_running = false;
var world; // represents the model, the simulation and the UI state
var mouse_drag_start;
var world_drag_start;
//var handle_drag_start;
var item_drag_start;
var drag_type;
var disable_drawing = false; //set to true when running the solver
var is_embedded = false;
var verlet_init_done = false;

var world_changed = function (newValue) {
  if (!disable_drawing) {
    draw_world();
  }
};

function physics(e) {
  if( self != top ) {
    //console.log("running in an iframe: " + self.src);
    is_embedded = true;
    //var headTag = document.getElementsByTagName("head")[0].innerHTML;
    //var frameCSS = headTag + '<style type="text/css" src="embed.css"></style>';
    //document.getElementsByTagName('head')[0].innerHTML = frameCSS;
    document.getElementById("left").style.display = "none";
    document.getElementById("right").style.display = "none";
  } else {
    document.getElementById("embed_tools").style.display = "none";
  }
  canvas = document.getElementById(e);
  context = canvas.getContext('2d');
  if (!context.setLineDash) {
    context.setLineDash = function () {};
  };
  
  canvas.addEventListener('click', canvas_click_handler, false);
  canvas.addEventListener("mousedown", mousedown_handler, false);
  window.addEventListener('resize', resizeCanvas, false);
  
  load_world(window.world_data);
  resizeCanvas();
  canvas.style.backgroundColor = world.color();
  ko.applyBindings(world, document.getElementById('right'));
  ko.applyBindings(world, document.getElementById('canvas_div'));
}

function load_world(data){
  world = ko.mapping.fromJSON(data);
  
  context_world_transform();
  attach_subscriber(world,["bodies","forces","joints","misc","selected_item","space_scale","center.x","center.y","scene.show_crosshair","scene.show_grid"],world_changed);
      
  world.color.subscribe(function(newValue) {
      canvas.style.backgroundColor = newValue;
  });

  for(var i = 0; i < world.bodies().length; i++){
    attach_subscriber(world.bodies()[i],["color","position.x","position.y","velocity.x","velocity.y"],world_changed);

    if(world.bodies()[i].type() == 'disk')
      attach_subscriber(world.bodies()[i],["radius","angle"],world_changed);

    if(world.bodies()[i].type() == 'box')
      attach_subscriber(world.bodies()[i],["width","height","angle"],world_changed);
  };
  
  
  for(var i = 0; i < world.forces().length; i++){
    if(world.forces()[i].type() == 'spring')
      attach_subscriber(world.forces()[i],["color","position_1.x","position_1.y","position_2.x","position_2.y", "body_1","body_2"],world_changed);
    if(world.forces()[i].type() == 'linear_motor')
      attach_subscriber(world.forces()[i],["color","position.x","position.y","force.x","force.y","body"],world_changed);
    if(world.forces()[i].type() == 'circular_motor')
      attach_subscriber(world.forces()[i],["color","position.x","position.y","torque","body"],world_changed);
  };
  
  for(var i = 0; i < world.joints().length; i++){
    var j = world.joints()[i];
    if(j.type() == 'anchor')
      attach_subscriber(j,["color","position.x","position.y"],world_changed);
    if(j.type() == 'pin')
      attach_subscriber(j,["color","position.x","position.y"],world_changed);
  };
  
  for(var i = 0; i < world.misc().length; i++){
    var m = world.misc()[i];
    if(m.type() == 'note')
      //attach_subscriber(world.forces()[i],["color","position_1.x","position_1.y","position_2.x","position_2.y", "body_1","body_2"],world_changed);
      var x = 0;
    if(m.type() == 'tracer')
      attach_subscriber(m,["color","position.x","position.y"],world_changed);
    if(m.type() == 'meter')
      attach_subscriber(m,["color","position.x","position.y","width","height"],world_changed);
  };
};

function bounding_shape_for_item(item){
  if(item.type() == 'disk')
    return ['circle',{center: {x: item.position.x(), y: item.position.y()}, radius: item.radius()}];
  if(item.type() == 'box')
    return ['rect',{center: {x: item.position.x(), y: item.position.y()}, width: item.width(), height: item.height(), angle: item.angle()}];
  if(item.type() == 'particle')
    return ['circle',{center: {x: item.position.x(), y: item.position.y()}, radius: world.scene.theme.size.particle() * world.space_scale()}];
  if(item.type() == 'linear_motor')
    return ['circle',{center: {x: item.position.x(), y: item.position.y()}, radius: world.scene.theme.size.linear_motor() * world.space_scale()}]; 
  if(item.type() == 'circular_motor')
    return ['circle',{center: {x: item.position.x(), y: item.position.y()}, radius: world.scene.theme.size.circular_motor() * world.space_scale()}];
  if(item.type() == 'spring'){
    var tmp1 = find_body_by_name(item.body_1());
    var tmp2 = find_body_by_name(item.body_2());
    var b1 = tmp1[0];
    var b2 = tmp2[0];
    var p1x = b1 ? b1.position.x() : item.position_1.x();
    var p1y = b1 ? b1.position.y() : item.position_1.y();
    var p2x = b2 ? b2.position.x() : item.position_2.x();
    var p2y = b2 ? b2.position.y() : item.position_2.y();
    //var p1 = item.position_1;
    //var p2 = item.position_2;
    var d = {x: (p2x - p1x), y: (p2y - p1y)};
    var a = Math.atan2(d.y,d.x);
    var l = Math.sqrt(d.x*d.x + d.y*d.y);
    return ['rect',{center: {x: 0.5*(p1x + p2x), y: 0.5*(p1y + p2y)}, width: l, height: 30*world.space_scale(), angle: a}];
  }
  if(item.type() == 'anchor')
    return ['circle',{center: {x: item.position.x(), y: item.position.y()}, radius: world.scene.theme.size.anchor() * world.space_scale()}];
  if(item.type() == 'pin')
    return ['circle',{center: {x: item.position.x(), y: item.position.y()}, radius: world.scene.theme.size.pin() * world.space_scale()}];
  if(item.type() == 'tracer')
    return ['circle',{center: {x: item.position.x(), y: item.position.y()}, radius: world.scene.theme.size.tracer() * world.space_scale()}];
  //if(item.type() == 'meter')
  //  return ['circle',{center: {x: item.position.x(), y: item.position.y()}, radius: ??? * world.space_scale()}];
  return undefined;
};

function show_notice(msg){
  document.getElementById('notice').innerHTML = msg;
  $("#notice").show();
}

function toggle_popup(elemid){
  $("#overlay").toggle();
  $("#"+elemid).toggle();
}

function toolbar_clicked(action){
  window.user_model.selected_action(action);
  if(action == 'select'){
    //current_action = undefined;
    show_notice("Click on an object to select.");
  } else if(action == 'add_particle'){
    show_notice("Click on the scene to add a particle");
  } else if(action == 'add_disk'){
    show_notice("Click on the scene to add a disk");
  } else if(action == 'add_box'){
    show_notice("Click on the scene to add a box");
  } else if(action == 'add_plane'){
    show_notice("Click on the scene to add a plane");
  } else if(action == 'add_constant_force'){
    toggle_popup('add_constant_force_popup');
  } else if(action == 'add_weight_force'){
    toggle_popup('add_weight_force_popup');
  } else if(action == 'add_custom_force'){
    toggle_popup('add_custom_force_popup');
  } else if(action == 'add_spring'){
    show_notice("Click on the scene to add a spring");
  } else if(action == 'add_linear_motor'){
    show_notice("Click on the scene to add a linear motor");
  } else if(action == 'add_circular_motor'){
    show_notice("Click on the scene to add a circular motor");
  } else if(action == 'add_note'){
    show_notice("Click on the scene to add a note");
  } else if(action == 'add_tracer'){
    show_notice("Click on the scene to add a tracer");
  } else if(action == 'add_meter'){
    show_notice("Click on the scene to add a meter");
  } else if(action == 'add_anchor'){
    show_notice("Click on the scene to add an anchor");
  } else if(action == 'add_pin'){
    show_notice("Click on the scene to add a pin");
  } else if(action == 'add_stick'){
    show_notice("Click on the scene to add a stick");
  }  else if(action == 'simulate'){
    if(is_embedded){
      document.getElementById('embed_simulate').style.display = 'none';
      document.getElementById('embed_stop').style.display = 'block';
    } else {
      document.getElementById('simulate').style.display = 'none';
      document.getElementById('stop').style.display = 'block';
    }
    is_running = true;
    schedule_next_timestep(1);
  } else if(action == 'stop'){
    if(is_embedded){
      document.getElementById('embed_simulate').style.display = 'block';
      document.getElementById('embed_stop').style.display = 'none';      
    } else {
      document.getElementById('simulate').style.display = 'block';
      document.getElementById('stop').style.display = 'none';
    }
    is_running = false;
  } else if(action == 'zoom_in'){
    world.space_scale(world.space_scale() / 2.0);
  } else if(action == 'zoom_out'){
    world.space_scale(world.space_scale() * 2.0);
  };
  return false; // prevent default event processing - did not work
};

function get_item(item){
  var type,index;
  if(!item)
    return undefined;
  var arr = item.split('_');
  type = arr[0];
  index = arr[1];
  if(type == 'body')
    return world.bodies()[index];
  else if(type =='force')
    return world.forces()[index];
  else if(type =='misc')
    return world.misc()[index];
  else if(type == 'joint')
    return world.joints()[index];
};

function remove_selected_item(){
  var type,index;
  var arr = world.selected_item().split('_');
  type = arr[0];
  index = arr[1];
  if(type == 'body')
    world.bodies.remove(world.bodies()[index]);
  else if(type =='force')
    world.forces.remove(world.forces()[index]);
  else if(type =='joint')
    world.joints.remove(world.joints()[index]);
  else if(type =='misc')
    world.misc.remove(world.misc()[index]);
  world.selected_item(undefined);
};

function eval_force(force,x,y,t,m,c){
  if(force.type() == "constant")
    return { x: force.x(), y: force.y()};
  else if(force.type() == "weight")
    return { x: 0.0, y: (-1.0 * m * force.g())};
  else if (force.type() == "custom") {
    var func = new Function('px','py','t','m','c',force.code());
    return func(x,y,t,m,c);
  };
};

function attach_subscriber(obj,props,fn){
  for(var i = 0; i < props.length; i++){
    var p = props[i].split('.');
    if(p.length == 1)
      obj[p[0]].subscribe(fn);
    else
      obj[p[0]][p[1]].subscribe(fn);
  };
};

function get_available_name(type){
  var collection = { particle: "bodies", disk: "bodies", box: "bodies",
    constant: "forces", weight: "forces", custom: "forces",
    linear_motor: "forces", circular_motor: "forces", spring: "forces",
    note: "misc", tracer: "misc", meter: "misc", anchor: "joints", pin: "joints",
    stick: "joints", plane: "misc"};
  var ctype = collection[type]
  var mx = 0;
  for(var i = 0; i < world[ctype]().length; i++){
    var item = world[ctype]()[i];
    if(item.type() == type && item.name().startsWith(type+"_")){
      var num = item.name().split("_")[1];
      if(isNaN(num))
        continue;
      else
        mx = Math.max(mx,Number(num));
    }
  };
  return (type + "_" + (++mx));
}

function add_particle(wp) {
    //console.log("mouse coordinates are: " + cx + "," + cy);
    var wx = wp.x; //world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
    var wy = wp.y; //world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
    var name = get_available_name("particle");
    var p = {type: ko.observable("particle"), mass: ko.observable(1), position: {x: ko.observable(wx), y: ko.observable(wy)}, velocity: {x: ko.observable(0), y: ko.observable(0)}, acceleration: {x: ko.observable(0), y: ko.observable(0)}, "name": ko.observable(name), charge: ko.observable(0.0), color: ko.observable(world.scene.theme.colors.particle())};
    world.bodies.push(p); // using the KnockOut version of push instead of javascript-native verrion: w.bodies().push(p)
    attach_subscriber(p,["color","position.x","position.y","velocity.x","velocity.y"],world_changed);
    world.selected_item('body_' + (world.bodies().length-1));
    //console.log("particle added at " + wx + "," + wy);
}

function add_note(cx,cy){
  var n = {name: ko.observable(get_available_name("note")), type: ko.observable("note"), x: ko.observable(String(cx)+"px"), y: ko.observable(String(cy)+"px"), color: ko.observable(world.scene.theme.colors.note()), html: ko.observable("<b>This is a note</b>"), size: {x: ko.observable("100px"), y: ko.observable("60px")}};
  world.misc.push(n);
}

function add_tracer(wp){
  var wx = wp.x; //world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
  var wy = wp.y; //world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
  var n = {name: ko.observable(get_available_name("tracer")), type: ko.observable("tracer"), position: {x: ko.observable(wx), y: ko.observable(wy)}, color: ko.observable(world.scene.theme.colors.tracer()), body: ko.observable(undefined), local_position: {x: ko.observable(0), y: ko.observable(0)}, points: ko.observable([])};
  world.misc.push(n);
  attach_subscriber(n,["color","position.x","position.y"],world_changed);
}

function add_plane(wp,ang){
  console.log("Adding a plane at x = " + wp.x + ", y = " + wp.y);
  var p = { name: ko.observable(get_available_name("plane")), type: ko.observable("plane"), position: {x: ko.observable(wp.x), y: ko.observable(wp.y)}, color: ko.observable(world.scene.theme.colors.plane()), angle: ko.observable(ang) };
  world.misc.push(p);
  attach_subscriber(p,["color","position.x","position.y","angle"],world_changed);
}

function add_anchor(wp){
  var wx = wp.x;//world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
  var wy = wp.y;//world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
  var n = {name: ko.observable(get_available_name("anchor")), type: ko.observable("anchor"), position: {x: ko.observable(wx), y: ko.observable(wy)}, color: ko.observable(world.scene.theme.colors.anchor()), body: ko.observable(undefined), angle: ko.observable(0)};
  world.joints.push(n);
  attach_subscriber(n,["color","position.x","position.y"],world_changed);
}

function add_pin(wp){
  var wx = wp.x;//world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
  var wy = wp.y;//world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
  var n = {name: ko.observable(get_available_name("pin")), type: ko.observable("pin"), position: {x: ko.observable(wx), y: ko.observable(wy)}, color: ko.observable(world.scene.theme.colors.pin()), body: ko.observable(undefined), local_position: {x: ko.observable(0), y: ko.observable(0)}};
  world.joints.push(n);
  attach_subscriber(n,["color","position.x","position.y"],world_changed);
}

function add_meter(wp){
  var wx = wp.x; //world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
  var wy = wp.y; //world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
  var n = {name: ko.observable(get_available_name("meter")), type: ko.observable("meter"), position: {x: ko.observable(wx), y: ko.observable(wy)}, color: ko.observable("magenta"), object: ko.observable("Sun"), property: ko.observable("mass"), width: ko.observable(80), height: ko.observable(60)};
  world.misc.push(n);
  attach_subscriber(n,["color","position.x","position.y","width","height"],world_changed);
}

function add_spring(wp){
    var wx1 = wp.x;//world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
    var wy1 = wp.y;//world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
    var wx2 = wp.x + 50 * world.space_scale(); //world.center.x() + (cx + 50 - (canvas.width/2)) * world.space_scale();
    var wy2 = wp.y + 50 * world.space_scale(); //world.center.y() + ((canvas.height/2) - cy - 50) * world.space_scale();
    var spring = {
      type: ko.observable("spring"),
      name: ko.observable(get_available_name("spring")),
      color: ko.observable("white"),
      position_1: {x: ko.observable(wx1), y: ko.observable(wy1)},
      position_2: {x: ko.observable(wx2), y: ko.observable(wy2)},
      stiffness: ko.observable(1),
      damping: ko.observable(0),
      rest_length: ko.observable(70 * world.space_scale()),
      body_1: ko.observable(undefined),
      body_2: ko.observable(undefined)
    };
    world.forces.push(spring);
    attach_subscriber(spring,["color","position_1.x","position_1.y","position_2.x","position_2.y","body_1","body_2"],world_changed);
};

function add_stick(wp){
    var wx1 = wp.x;//world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
    var wy1 = wp.y;//world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
    var wx2 = wp.x + 50 * world.space_scale(); //world.center.x() + (cx + 50 - (canvas.width/2)) * world.space_scale();
    var wy2 = wp.y + 50 * world.space_scale(); //world.center.y() + ((canvas.height/2) - cy - 50) * world.space_scale();
    var stick = {
      type: ko.observable("stick"),
      name: ko.observable(get_available_name("stick")),
      color: ko.observable("blue"),
      position_1: {x: ko.observable(wx1), y: ko.observable(wy1)},
      position_2: {x: ko.observable(wx2), y: ko.observable(wy2)},
      body_1: ko.observable(undefined),
      body_2: ko.observable(undefined),
      rest_length: ko.observable(50 * 1.414 * world.space_scale())
    };
    world.joints.push(stick);
   attach_subscriber(stick,["color","position_1.x","position_1.y","position_2.x","position_2.y","body_1","body_2", "rest_length"],world_changed);
};

function add_linear_motor(wp){
    var wx = wp.x;//world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
    var wy = wp.y;//world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
    var motor = {
      type: ko.observable("linear_motor"),
      name: ko.observable(get_available_name("linear_motor")),
      color: ko.observable("pink"),
      position: {x: ko.observable(wx), y: ko.observable(wy)},
      force: {x: ko.observable(0), y: ko.observable(0)},
      body: ko.observable(undefined),
      local_position: {x: ko.observable(0), y: ko.observable(0)}
    };
    world.forces.push(motor);
    attach_subscriber(motor,["color","position.x","position.y","force.x","force.y","body"],world_changed);
};

function add_circular_motor(wp){
    var wx = wp.x;//world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
    var wy = wp.y;//world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
    var motor = {
      type: ko.observable("circular_motor"),
      name: ko.observable(get_available_name("circular_motor")),
      color: ko.observable("red"),
      position: {x: ko.observable(wx), y: ko.observable(wy)},
      torque: ko.observable(0),
      body: ko.observable(undefined)
    };
    world.forces.push(motor);
    attach_subscriber(motor,["color","position.x","position.y","torque","body"],world_changed);
};

function add_disk(wp) {
    var wx = wp.x;//world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
    var wy = wp.y;//world.center.y() + ((canvas.height/2) - cy) * world.space_scale();
    
    var disk = {
      type: ko.observable("disk"), 
      radius: ko.observable(50 * world.space_scale()), 
      angle: ko.observable(0.0), 
      inertia: ko.observable(1), 
      mass: ko.observable(1), 
      position: {x: ko.observable(wx), y: ko.observable(wy)}, 
      velocity: {x: ko.observable(0), y: ko.observable(0)}, 
      angular_velocity: ko.observable(0.0), 
      angular_acceleration: ko.observable(0.0), 
      acceleration: {x: ko.observable(0), y: ko.observable(0)}, 
      name: ko.observable(get_available_name("disk")), 
      charge: ko.observable(0.0), 
      color: ko.observable("orange")
    };
    
    world.bodies.push(disk);
    attach_subscriber(disk,["color","position.x","position.y","velocity.x","velocity.y","radius","angle","angular_velocity"],world_changed);
    world.selected_item('body_' + (world.bodies().length-1));
};

function add_box(wp) {
    var wx = wp.x;//world.center.x() + (cx - (canvas.width/2)) * world.space_scale();
    var wy = wp.y;//world.center.y() + ((canvas.height/2) - cy) * world.space_scale();

    var box = {
      type: ko.observable("box"), 
      width: ko.observable(80 * world.space_scale()), 
      height: ko.observable(40 * world.space_scale()), 
      angle: ko.observable(0.0), 
      inertia: ko.observable(1), 
      mass: ko.observable(1), 
      position: {x: ko.observable(wx), y: ko.observable(wy)}, 
      velocity: {x: ko.observable(0), y: ko.observable(0)}, 
      angular_velocity: ko.observable(0.0), 
      angular_acceleration: ko.observable(0.0), 
      acceleration: {x: ko.observable(0), y: ko.observable(0)}, 
      name: ko.observable(get_available_name("box")), 
      charge: ko.observable(0.0), 
      color: ko.observable("red")
    };
    
    world.bodies.push(box);
    attach_subscriber(box,["color","position.x","position.y","velocity.x", "velocity.y","width","height","angle","angular_velocity"],world_changed);
    world.selected_item('body_' + (world.bodies().length-1));
};

function add_constant_force(x,y){
  //console.log("adding new constant force: " + x + "," + y);
  var name = get_available_name("constant");
  world.forces.push({"name": ko.observable(name), type: ko.observable("constant"), "x": ko.observable(x), "y": ko.observable(y)});
  document.getElementById('add_constant_force_popup').style.display = 'none';
  document.getElementById('overlay').style.display = "none";
  toolbar_clicked('select');
};

function add_weight_force(g){
  //console.log("adding new weight force: " + g);
  var name = get_available_name("weight");
  world.forces.push({"name": ko.observable(name), type: ko.observable("weight"), "g": ko.observable(g)});
  document.getElementById('add_weight_force_popup').style.display = 'none';
  document.getElementById('overlay').style.display = "none";
  toolbar_clicked('select');
};

function add_custom_force(code){
  //console.log("adding new force: " + code);
  var name = get_available_name("custom");
  world.forces.push({"name": ko.observable(name), type: ko.observable("custom"), "code": ko.observable(code)});
  document.getElementById('add_custom_force_popup').style.display = 'none';
  document.getElementById('overlay').style.display = "none";
  toolbar_clicked('select');
};

function vector_difference(vec1, vec2){
  return {x: (vec2.x() - vec1.x()), y: (vec2.y() - vec1.y())};
}

function is_rigid(type){
  if(type() == 'disk' || type() == 'box')
    return true;
  else
    return false;
}

function find_body_by_name(name){
  if(!name)
    return [undefined, undefined];
  for(var i = 0; i < world.bodies().length; i++){
    var body = world.bodies()[i];
    if(body.name() == name)
      return [body,i];
  };
  return [undefined, undefined]
};

function update_all_tracers(){
  for(var i = 0; i < world.misc().length; i++){
    var t = world.misc()[i];
    if(t.type() != 'tracer')
      continue;
    var b = find_body_by_name(t.body())[0];
    if(!b)
      continue;
    //console.log("found body for tracer");
    t.points().push([b.position.x(),b.position.y()]);
    t.position.x(b.position.x());
    t.position.y(b.position.y());
  }
}

function particle_particle_collision(b1,b2){
  //should return false
  var tolerance = 0.1;
  var d = {x: b2.position.x() - b1.position.x(), y: b2.position.y() - b1.position.y()};
  var r = Math.sqrt(d.x*d.x + d.y*d.y);
  if(r <= tolerance)
    return true;
}

function particle_disk_collision(b1,b2){
  var d = {x: b2.position.x() - b1.position.x(), y: b2.position.y() - b1.position.y()};
  var r = Math.sqrt(d.x*d.x + d.y*d.y);
  if(r <= b2.radius())
    return true;
}

function particle_box_collision(b1,b2){
  var p = {x: b1.position.x() - b2.position.x(), y: b1.position.y() - b2.position.y()};
  var a = Math.atan2(p.y,p.x);
  var r = Math.sqrt(d.x*d.x + d.y*d.y);
  var pv = {x: r*Math.cos(a-b2.angle()), y: r*Math.sin(a-b2.angle())};
  if(Math.abs(pv.x) < b2.width()/2 && Math.abs(pv.x) < b2.height()/2)
    return true;
}

function disk_disk_collision(b1,b2){
  var d = {x: b2.position.x() - b1.position.x(), y: b2.position.y() - b1.position.y()};
  var r = Math.sqrt(d.x*d.x + d.y*d.y);
  if(r <= b1.radius() + b2.radius())
    return true;
}

function disk_box_collision(b1,b2){
}

function box_box_collision(b1,b2){
}

function detect_collisions(){
  for(var i = 0; i < world.bodies()[i]; i++){
    for(var j = i+1; j < world.bodies()[j]; j++){
      var b1 = world.bodies()[i];
      var b2 = world.bodies()[j];
      if(b1.type() == 'particle')
        if(b2.type() == 'particle')
          particle_particle_collision(b1,b2);
        else if(b2.type() == 'disk')
          particle_disk_collision(b1,b2);
        else if(b2.type() == 'box')
          particle_box_collision(b1,b2);
      else if(b1.type() == 'disk')
        if(b2.type() == 'particle')
          particle_disk_collision(b2,b1);
        else if(b2.type() == 'disk')
          disk_disk_collision(b1,b2);
        else if(b2.type() == 'box')
          disk_box_collision(b1,b2);
      else if(b1.type() == 'box')
        if(b2.type() == 'particle')
          particle_box_collision(b2,b1);
        else if(b2.type() == 'disk')
          disk_box_collision(b2,b1);
        else if(b2.type() == 'box')
          box_box_collision(b1,b2);
    }
  }
}

function get_world_state_and_varoffsets(){
  var state = []; var varoffsets = []; var counter = 0;
  for(var i = 0; i < world.bodies().length; i++){
    var b = world.bodies()[i];
    state.push(b.position.x());
    state.push(b.position.y());
    if(is_rigid(b.type))
      state.push(b.angle());

    state.push(b.velocity.x());
    state.push(b.velocity.y());
    if(is_rigid(b.type))
      state.push(b.angular_velocity());
      
    varoffsets.push(counter);
    if(is_rigid(b.type)) {
      counter += 6; //disks and boxes
    } else {
      counter += 4; //particle
    }
  }
  return [state, varoffsets];
}

function set_world_state(state, deriv, varoffsets){
  // we use deriv to set acceleration values in world so that arrows can be shown on canvas.
  for(var i = 0; i < world.bodies().length; i++){
    var b = world.bodies()[i];
    var voff = varoffsets[i];
    b.position.x(state[voff + 0]);
    b.position.y(state[voff + 1]);
    b.velocity.x(state[voff + (is_rigid(b.type) ? 3 : 2)]);
    b.velocity.y(state[voff + (is_rigid(b.type) ? 4 : 3)]);
    
    if(is_rigid(b.type)){
      b.angle(state[voff + 2]);
      b.angular_velocity(state[voff + 5]);
    }
    b.acceleration.x(deriv[voff+(is_rigid(b.type) ? 3 : 2)]);
    b.acceleration.y(deriv[voff+(is_rigid(b.type) ? 4 : 3)]);
    if(is_rigid(b.type))
      b.angular_acceleration(deriv[voff+5]);
  }
}

function calculate_accelerations(state,t, varoffsets){
  // returns an array of objects containing x-accn, y-accn and angular accn: {x: 1, y: 3, alpha: 0.4}
  var result = [];
  for(var i = 0; i < world.bodies().length; i++){
    var b = world.bodies()[i];
    var offset = varoffsets[i];
    result[i] = {x: 0, y: 0, alpha: 0};
    var px = state[offset + 0]; // position from state
    var py = state[offset + 1];
    var vx = state[offset + (is_rigid(b.type) ? 3 : 2)];
    var vy = state[offset + (is_rigid(b.type) ? 4 : 3)];
    if(is_rigid(b.type)){
      //var angle = state[offset + 2];
      //var alpha = state[offset + 5];
    }
    // acceleration due to force fields if any present
    for(var j = 0; j < world.forces().length; j++){
      var force = world.forces()[j];
      if(force.type() == 'constant' || force.type() == 'weight' || force.type() == 'custom'){
        var force_value = eval_force(force,px, py, t, b.mass(), b.charge());
        result[i].x += force_value.x / b.mass();
        result[i].y += force_value.y / b.mass();

      } else if(force.type() == 'linear_motor'){
        if(force.body() == b.name()){
          var force_value = force.force;
          result[i].x += force_value.x() / b.mass();
          result[i].y += force_value.y() / b.mass();
        };
      } else if(force.type() == 'circular_motor'){
        if(is_rigid(b.type) && force.body() == b.name()){
          var torque_value = force.torque();
          result[i].alpha += torque_value / b.inertia();
        };
      } else if(force.type() == 'spring'){
        //springs and other n-ary forces should be processed outside this loop because two bodies are involved
      };
    };
	
    if(world.laws.near_earth_gravity()){
		  result[i].y -= world.laws.earth_g();
	  }
    
    // calculate pair-wise gravitation and electric forces
    for(var k = 0; k < world.bodies().length; k++){ 
      if(k == i)
        continue;
      var other_body = world.bodies()[k];
      var d = {x: (state[varoffsets[k] + 0] - px), y: (state[varoffsets[k] + 1] - py)}; //vector_difference(body.position, other_body.position); //fixme: one of these may be old value while the other new
      var r = Math.sqrt(d.x*d.x + d.y*d.y);
      var d_unit = {x: (d.x / r), y: (d.y / r)};
      if(world.laws.gravity()){
        var g_force_amp = world.laws.gravity_constant() * b.mass() * other_body.mass() / (r*r);
        //console.log("g_force_amp = "); console.log(g_force_amp);
        var g_force = {x: (g_force_amp * d_unit.x), y: (g_force_amp * d_unit.y)};
        result[i].x += g_force.x / b.mass();
        result[i].y += g_force.y / b.mass();
        // todo - we should update other_body 's acceleration here as well to avoid recalculation
      }
      if(world.laws.electric()){
        var e_force_amp = (-1.0) * world.laws.electric_constant() * b.charge() * other_body.charge() / (r*r);
        var e_force = {x: (e_force_amp * d_unit.x), y: (e_force_amp * d_unit.y)};
        result[i].x += e_force.x / b.mass();
        result[i].y += e_force.y / b.mass();
      }
    }
  }; //loop over all bodies
  
  for(var j = 0; j < world.forces().length; j++){
    var force = world.forces()[j];
    if(force.type() == 'spring'){ // others have been processed above
      var tmp1 = find_body_by_name(force.body_1());
      var tmp2 = find_body_by_name(force.body_2());
      var b1 = tmp1[0], b1index = tmp1[1];
      var b2 = tmp2[0], b2index = tmp2[1];
/*      if(b1){
        var oldx = force.position_1.x();
        var oldy = force.position_1.y();
        p1_x = b1.position.x();
        p1_y = b1.position.y();
        if(!b2){
          p2_x = force.position_2.x() + b1.position.x() - oldx;
          p2_y = force.position_2.y() + b1.position.y() - oldy;
        };
      };
      if(b2){
        var oldx = force.position_2.x();
        var oldy = force.position_2.y();
        force.position_2.x(b2.position.x());
        force.position_2.y(b2.position.y());
        if(!b1){
          force.position_1.x(force.position_1.x() + b2.position.x() - oldx);
          force.position_1.y(force.position_1.y() + b2.position.y() - oldy);
        };
      }; */
      if(b1 && b2){
        var lx = state[varoffsets[b2index] + 0] - state[varoffsets[b1index] + 0];
        var ly = state[varoffsets[b2index] + 1] - state[varoffsets[b1index] + 1];
        var length = Math.sqrt(lx*lx + ly*ly);
        var angle = Math.atan2(ly,lx);
        var f = (length - force.rest_length()) * force.stiffness(); //todo - damping using relative velocity
        //console.log("calculated spring force = " + f);
        result[b1index].x += f*Math.cos(angle)/b1.mass();
        result[b1index].y += f*Math.sin(angle)/b1.mass();
        result[b2index].x -= f*Math.cos(angle)/b2.mass();
        result[b2index].y -= f*Math.sin(angle)/b2.mass();
      };
    };
  };
  return result;
}

function get_world_deriv(initial,t,dt,deriv,varoffsets){
  // dt is between 0 and delta_t
  var result = []; //contains velocities and accelerations in same order as state variables
  var state = euler_step(initial,deriv,dt);
  var accn = calculate_accelerations(state,t+dt, varoffsets); // returns an array of objects containing x-accn, y-accn and angular accn: {x: 1, y: 3, alpha: 0.4}
  for(var i = 0; i < world.bodies().length; i++){
    var b = world.bodies()[i];
    var offset = varoffsets[i];
    result.push(state[offset + (is_rigid(b.type) ? 3 : 2)]); //x-velocity of b in state
    result.push(state[offset + (is_rigid(b.type) ? 4 : 3)]); //y-velocity of b in state
    if(is_rigid(b.type))
      result.push(state[offset + 5]); //omega

    result.push(accn[i].x);
    result.push(accn[i].y);

    if(is_rigid(b.type))
      result.push(accn[i].alpha); // angular acceleration for body b
  }
  return result;
};

function euler_step(state,deriv,dt){
  var new_state = []
  for(var i = 0; i < state.length; i++){
    new_state[i] = state[i] + (deriv[i] ? deriv[i] : 0) * dt;
  }
  return new_state;
};

function schedule_next_timestep(delay){
  if(!delay)
    delay = world.frame_delay();

  if(is_running) {
    if(world.solver.type() == "euler")
      setTimeout(function () {
        generic_simulate(euler_deriv);
      }, delay);
    else if(world.solver.type() == "rk4")
      setTimeout(function () {
        generic_simulate(rk4_deriv);
      }, delay);
    else if(world.solver.type() == "verlet") // http://www.gotoandplay.it/_articles/2005/08/advCharPhysics.php
      setTimeout(function () {
        verlet_simulate();
      }, delay);
    else if(world.solver.type() == "leapfrog")
      setTimeout(function () {
        leapfrog_simulate();
      }, delay);
  }
}

function get_constraint_info_for(joint,jindex,state,c,cderiv,j,jderiv,offset,varoffsets){
  if(joint.type() == 'anchor'){
    var tmp = find_body_by_name(joint.body());
    var b = tmp[0]; var bindex = tmp[1];
    if(b){
      var voff = varoffsets[bindex];
      c[offset] = state[voff+0] - joint.position.x();
      c[offset + 1] = state[voff+1] - joint.position.y();
      if(is_rigid(b.type))
        c[offset + 2] = state[voff+2] - joint.angle();
      cderiv[offset] = state[voff+(is_rigid(b.type) ? 3 : 2)]; //vx
      cderiv[offset + 1] = state[voff+(is_rigid(b.type) ? 4 : 3)]; //vy
      if(is_rigid(b.type))
        cderiv[offset + 2] = state[voff+5]; //omega
      j[offset][voff/2+0] = 1; //constraint on x
      j[offset+1][voff/2+1] = 1; //constraint on y
      if(is_rigid(b.type))
        j[offset+2][voff/2+2] = 1; //constraint on angle
    }
  } else if(joint.type() == 'pin'){
      var tmp = find_body_by_name(joint.body());
      var b = tmp[0]; var bindex = tmp[1];
      if(b){
        var voff = varoffsets[bindex];
        var lp = joint.local_position;
        if(is_rigid(b.type)){
          var a = state[voff+2]; //angle
          var r1 = {x: (lp.x()*Math.cos(a)-lp.y()*Math.sin(a)), y: (lp.x()*Math.sin(a)+lp.y()*Math.cos(a))};
          var p1 = {x: (state[voff+0]+r1.x), y: (state[voff+1]+r1.y)};
          var av = state[voff + 5]; //angular velocity of the body around CM
          var v1 = {x: (state[voff+3]-av*r1.y), y: (state[voff+4] + av*r1.x)};
  
          //populate c and cderiv correctly
          c[offset] = p1.x - joint.position.x();
          c[offset+1] = p1.y - joint.position.y();
          cderiv[offset] = v1.x;
          cderiv[offset+1] = v1.y;
          j[offset][voff/2+0] = 1;
          j[offset][voff/2+2] = -r1.y; // rotation affects x-coord of pin
          j[offset+1][voff/2+1] = 1;
          j[offset+1][voff/2+2] = r1.x; // rotation changes y coordinate of pin
          jderiv[offset][voff/2+2] = -av*r1.x;
          jderiv[offset+1][voff/2+2] = -av*r1.y;
        } else { //particle
          var d = lp.x()*lp.x() + lp.y()*lp.y();
          var r = {x: (state[voff+0]-joint.position.x()),y: (state[voff+1]-joint.position.y())};
          if(d <= 0.01){
            c[offset] = r.x;
            c[offset+1] = r.y;
            cderiv[offset] = state[voff+2]; //vx
            cderiv[offset+1] = state[voff+3]; //vy
            j[offset][voff/2+0] = 1;
            j[offset+1][voff/2+1] = 1;
          } else {
            c[offset] = 0.5*(r.x*r.x+r.y*r.y-d);
            cderiv[offset] = state[voff+2]*r.x + state[voff+3]*r.y; //v dot r
            j[offset][voff/2+0] = r.x;
            j[offset][voff/2+1] = r.y;
            jderiv[offset][voff/2+0] = state[voff+2];
            jderiv[offset][voff/2+1] = state[voff+3];
          }
        }
      }
  } else if(joint.type() == 'stick'){
    var tmp1 = find_body_by_name(joint.body_1());
    var b1 = tmp1[0]; var b1index = tmp1[1];
    var tmp2 = find_body_by_name(joint.body_2());
    var b2 = tmp2[0]; var b2index = tmp2[1];
    if(!b1 || !b2)
      return;
    var voff1 = varoffsets[b1index];
    var voff2 = varoffsets[b2index];
    var p_rel = {x: (state[voff2+0] - state[voff1+0]), y: (state[voff2+1] - state[voff1+1])};
    var v_rel = {x: (state[voff2+(is_rigid(b2.type) ? 3 : 2)] - state[voff1+(is_rigid(b1.type) ? 3 : 2)]), y: (state[voff2+(is_rigid(b2.type) ? 4 : 3)] - state[voff1+(is_rigid(b1.type) ? 4 : 3)])};
    c[offset] = 0.5*((p_rel.x*p_rel.x + p_rel.y*p_rel.y) - joint.rest_length() * joint.rest_length());
    cderiv[offset] = p_rel.x*v_rel.x + p_rel.y*v_rel.y;
    j[offset][voff1/2+0] = -1 * p_rel.x;
    j[offset][voff1/2+1] = -1 * p_rel.y;
    //TODO: jderiv
  };
}

function num_constraints_for_joint(j){
  //TODO: sticks
  if(j.type() == 'anchor'){
    var tmp = find_body_by_name(j.body());
    var b = tmp[0]; var bindex = tmp[1];
    if(b) return (is_rigid(b.type) ? 3 : 2);
  } else if(j.type() == 'pin'){
    var tmp = find_body_by_name(j.body());
    var b = tmp[0]; var bindex = tmp[1];
    if(b) {
      if(is_rigid(b.type))
        return 2; //x,y
      else { //particle
        var lp = j.local_position;
        var d = lp.x()*lp.x() + lp.y()*lp.y();
        if(d <= 0.01) //epsilon
          return 2;
        else
          return 1;
      }
    }
  } else if(j.type() == 'stick'){
    var tmp1 = find_body_by_name(j.body_1());
    var b1 = tmp1[0]; var b1index = tmp1[1];
    var tmp2 = find_body_by_name(j.body_2());
    var b2 = tmp2[0]; var b2index = tmp2[1];
    if(b1 && b2){
      return 1; //distance between two bodies is same as rest_length of the stick
    } else {
      return 0;
    }
  };
  return 0;
}

function get_num_constraints(){
  var num = 0;
  for(var i = 0; i < world.joints().length; i++){
    var j = world.joints()[i];
    num += num_constraints_for_joint(j);
   }
  return num;
}

function zeros(a,b){
  var ret;
  if(a == 0) //scalar
    return 0;
  ret = new Array(a);
  for (var i = 0; i < a; i++) {
    ret[i] = (b ? zeros(b) : 0);
  }
  return ret;
}

function apply_constraints(state,deriv,drift_correction, varoffsets, velocity_model, delta_t, nconstraints){
  // velocities/accelerations need to be corrected because of constraints
  // we need to returned the corrected state & deriv vector after integration
  
  var nbodies = world.bodies().length;
  var nvars = state.length / 2; //number of total position variables
  var jacobian = zeros(nconstraints, nvars);
  var jacobianderiv = zeros(nconstraints, nvars);
  var c = zeros(nconstraints);
  var cderiv = zeros(nconstraints);
  var inversemass = zeros(nvars,nvars);

  for(var i = 0; i < nbodies; i++){
    var b = world.bodies()[i];
    var offset = varoffsets[i] / 2; //we need an offset just for position, ignoring velocity
    inversemass[offset+0][offset+0] = 1.0 / b.mass();
    inversemass[offset+1][offset+1] = 1.0 / b.mass();
    if(is_rigid(b.type))
      inversemass[offset+2][offset+2] = 1.0 / b.inertia();
  }
  for(var i = 0, counter = 0; i < world.joints().length; i++){
    var joint = world.joints()[i];
    get_constraint_info_for(joint,i,state,c,cderiv,jacobian,jacobianderiv,counter,varoffsets);
    counter += num_constraints_for_joint(joint);
  }

  var vel = []; // qdot - we need a vector containing just the velocities
  var new_accn = []; //qdoubledot - a vector containing just the accn
  var tmp;
  if(velocity_model){
    tmp = euler_step(state,deriv,delta_t); // we integrate velocities before applying constraint impulses.
  } else {
    tmp = state;
  }
  for(var i = 0; i < nbodies; i++){
    var b = world.bodies()[i];
    // pick velocities from tmp, not state or deriv because we need integrated values in case of velocity_model
    vel.push(tmp[varoffsets[i] + (is_rigid(b.type) ? 3 : 2)]); //vx
    vel.push(tmp[varoffsets[i] + (is_rigid(b.type) ? 4 : 3)]); //vy
    if(is_rigid(b.type))
      vel.push(tmp[varoffsets[i] + 5]); //omega

    new_accn.push(deriv[varoffsets[i] + (is_rigid(b.type) ? 3 : 2)]); //ax
    new_accn.push(deriv[varoffsets[i] + (is_rigid(b.type) ? 4 : 3)]); //ay
    if(is_rigid(b.type))
      new_accn.push(deriv[varoffsets[i] + 5]); //alpha
  }
  
  //console.log("new_accn=");console.log(new_accn);

  //console.log("inversemass = "); console.log(inversemass);
  //console.log("jacobian = "); console.log(jacobian);
  //console.log("c = "); console.log(c);
  //console.log("jacobianderiv = "); console.log(jacobianderiv);

  //var jmatrix = $M(jacobian);
  var jtranspose = numeric.transpose(jacobian);
  if(!velocity_model){
    // now we have inversemass, jacobian and jacobianderiv to calculate constraint accelerations and add to the calculated ones.
    var a = numeric.dot(jacobian,numeric.dot(inversemass,jtranspose));
    var bmat = numeric.sub(numeric.mul(-1,numeric.dot(jacobianderiv,vel)),numeric.dot(jacobian,new_accn));
    //console.log("a = ");console.log(a);
    //console.log("bmat = ");console.log(bmat);
    if(drift_correction){
      var newb = numeric.sub(numeric.sub(bmat,c),cderiv);
      var lambda = numeric.solve(a,newb); 
    } else {
      var lambda = numeric.solve(a,bmat); 
    }
 } else {
   //console.log("Using velocity model of constraints");
   var a = numeric.dot(jacobian,numeric.dot(inversemass,jtranspose));
   var bmat = numeric.mul(-1,numeric.dot(jacobian,vel)); // TODO: need to add bias velocity term in bmat in case of velocity constraints
   if(drift_correction){
     var newb = numeric.sub(bmat, numeric.mul(1.0,c)); //baumgarte stabilization factor = beta / delta_t
     var lambda = numeric.solve(a,newb); 
   } else {
     var lambda = numeric.solve(a,bmat); 
   }
 }
  //console.log("lambda = ");console.log(lambda);
  var f = numeric.dot(jtranspose,lambda); // these are the constraint forces / impulses
  //console.log("deriv = "); console.log(deriv);
  //console.log("constraint forces = "); console.log(f);
  var new_deriv = [];//new Array(deriv.length);

  if(!velocity_model){
    for(var i = 0; i < nbodies; i++){
      var b = world.bodies()[i];
      var voff = varoffsets[i];
      new_deriv.push(deriv[voff+0]); //vx
      new_deriv.push(deriv[voff+1]); //vy
      if(is_rigid(b.type))
        new_deriv.push(deriv[voff+2]); //omega
      var ax = deriv[voff+(is_rigid(b.type) ? 3 : 2)] + f[voff/2+0] / b.mass();
      var ay = deriv[voff+(is_rigid(b.type) ? 4 : 3)] + f[voff/2+1] / b.mass();
      new_deriv.push(ax);
      new_deriv.push(ay);
      //new_deriv.push(deriv[i*6+4]);
      if(is_rigid(b.type)){
        var alpha = deriv[voff+5] + f[voff/2+2] / b.inertia();
        new_deriv.push(alpha);
      }
    }
    var new_state = euler_step(state,new_deriv,delta_t);
    return [new_state, new_deriv];
 } else {
   // in this case, f contains constraint impulses
   // just correct velocities and return. Integration has already been done.
   //var new_state = [];
   for(var i = 0; i < nbodies; i++){
     var b = world.bodies()[i];
     var voff = varoffsets[i];
     
     var vx = vel[voff/2+0] + f[voff/2+0] / b.mass(); // apply constraint impulse
     var vy = vel[voff/2+1] + f[voff/2+1] / b.mass();
     new_deriv.push(vx);
     new_deriv.push(vy);
     if(is_rigid(b.type)) {
       var omega = vel[voff/2+2] + f[voff/2+2] / b.inertia();
       new_deriv.push(omega);
     }

     // accn is only required to show the values and arrows in UI. Not used for integration
     var ax = (vx - deriv[voff+0]) / delta_t; 
     var ay = (vy - deriv[voff+1]) / delta_t;
     new_deriv.push(ax);
     new_deriv.push(ay);
     if(is_rigid(b.type)){
       var alpha = (omega - deriv[voff+2]) / delta_t;
       new_deriv.push(alpha);
     }
   }
   var new_state = euler_step(state,new_deriv,delta_t);
   return [new_state, new_deriv];
 }
}

function euler_deriv(state, t, delta_t, varoffsets){
  return get_world_deriv(state,t,0.0,[], varoffsets);
}

function rk4_deriv(state, t, delta_t, varoffsets){
  var a = get_world_deriv(state,t,0.0,[], varoffsets);
  var b = get_world_deriv(state,t,delta_t*0.5,a, varoffsets);
  var c = get_world_deriv(state,t,delta_t*0.5,b, varoffsets);
  var d = get_world_deriv(state,t,delta_t,c, varoffsets);

  var deriv = []; // weighted average of a,b,c,d
  for(var i = 0; i < a.length; i++){
    deriv[i] = (1/6)*(a[i] + 2*b[i] + 2*c[i] + d[i]);
  }
  return deriv;
}

function verlet_simulate(){
  disable_drawing = true; // we'll be updating a whole lot of objects in the world. Let's draw manually once all updates are done
  var delta_t = world.solver.delta_t();
  var t = world.time();
  var tmp = get_world_state_and_varoffsets();
  var state = tmp[0]; var varoffsets = tmp[1];
  var accn = calculate_accelerations(state,t,varoffsets);
  if(!verlet_init_done){
    for(var i = 0; i < world.bodies().length; i++){
      var b = world.bodies()[i];
      var prevx = b.position.x() - b.velocity.x() * delta_t + 0.5 * accn[i].x * delta_t * delta_t;
      var prevy = b.position.y() - b.velocity.y() * delta_t + 0.5 * accn[i].y * delta_t * delta_t;
      b.prev_position = {x: prevx, y: prevy};
      if(is_rigid(b.type)){
        var prevtheta = b.angle() - b.angular_velocity() * delta_t + 0.5 * accn[i].alpha * delta_t * delta_t;
        b.prev_angle = prevtheta;
      }
    }
    verlet_init_done = true;
  }
  for(var i = 0; i < world.bodies().length; i++){
    var b = world.bodies()[i];
    var oldx = b.position.x(); var oldy = b.position.y();
    b.position.x(2*b.position.x() - b.prev_position.x + accn[i].x*delta_t*delta_t);
    b.position.y(2*b.position.y() - b.prev_position.y + accn[i].y*delta_t*delta_t);
    b.prev_position = {x: oldx, y: oldy};
    b.acceleration.x(accn[i].x);
    b.acceleration.y(accn[i].y);

    b.velocity.x((b.position.x() - oldx) / (2.0 * delta_t));
    b.velocity.y((b.position.y() - oldy) / (2.0 * delta_t));
    if(is_rigid(b.type)){
    var oldangle = b.angle();
      b.angle(2*b.angle() - b.prev_angle + accn[i].alpha*delta_t*delta_t);
      b.prev_angle = oldangle;
      b.angular_acceleration(accn[i].alpha);
      b.angular_velocity((b.angle() - oldangle) / (2.0 * delta_t));
    }
  }
  world.time(world.time() + delta_t);
  update_all_tracers();
  disable_drawing = false;
  draw_world();
  schedule_next_timestep();
}

function generic_simulate(deriv_method){
  disable_drawing = true; // we'll be updating a whole lot of objects in the world. Let's draw manually once all updates are done
  var delta_t = world.solver.delta_t();
  var t = world.time();
  var tmp = get_world_state_and_varoffsets();
  var state = tmp[0]; var varoffsets = tmp[1];
  var deriv = deriv_method(state, t, delta_t, varoffsets); // deriv_method is either euler_deriv() or rk4_deriv()
  var new_state = undefined;
  var new_deriv = undefined;
  var nconstraints = get_num_constraints();

  if(world.solver.apply_constraints() && nconstraints > 0){
    tmp = apply_constraints(state,deriv, world.solver.constraints_drift_correction(), varoffsets, world.solver.use_velocity_constraint_model(), delta_t, nconstraints);
    new_state = tmp[0];
    new_deriv = tmp[1];
  } else {
    new_deriv = deriv;
    new_state = euler_step(state,new_deriv,delta_t);
 }

  world.time(world.time() + delta_t);  
  set_world_state(new_state, new_deriv, varoffsets);
  update_all_tracers();
  disable_drawing = false;
  draw_world();
  schedule_next_timestep();
};

