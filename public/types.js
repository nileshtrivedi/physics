function Particle(type,name,color,mass,charge,px,py,vx,vy){
  this.type = ko.observable(type);
  this.name = ko.observable(name);
  this.color = ko.observable(color);
  this.mass = ko.observable(mass);
  this.charge = ko.observable(charge);
  this.position = {x: ko.observable(px), y: ko.observable(py)};
  this.velocity = {x: ko.observable(vx), y: ko.observable(vy)};
  this.force = 0;
  this.draw = function(context){
  };
  this.acceleration = ko.computed(function() {
        return this.force / this.mass();
    }, this);
}

Item draw()
 - Body
   - Particle
   - RigidBody
     - Disk
     - Box
     - Polygon?
 - Force apply()
   - Constant
   - Weight
   - Custom
   - Spring
   - Linear Motor
   - Circular Motor
 - Joint apply()?
   - Anchor
   - Pin
   - Stick
   - Rope?
   - Chain?
   - Pulley?
 - Watcher refresh()/update()
   - Note
   - Meter
   - Tracer
 - Law apply()
 - Solver
 - Scene
