function draw_world(){
  //console.log("drawing world");
  clear_world();
  for(var i = 0; i < world.bodies().length; i++){
    var body = world.bodies()[i];
    //console.log("drawing body : " + body.name());
    if(body.type() == "particle")
      draw_particle(body, world.selected_item() == "body_"+i);
    else if(body.type() == "disk")
      draw_disk(body,world.selected_item() == "body_"+i);
    else if(body.type() == "box")
      draw_box(body,world.selected_item() == "body_"+i);

    if(world.selected_item() == "body_"+i){
      var handles = handles_for_item(body);
      for(var j = 0; j < handles.length; j++){
        var h = handles[j];
        draw_handle(h.x,h.y);
      }
    }
  };
  for(var i = 0; i < world.forces().length; i++){
    var force = world.forces()[i];
    //console.log("drawing body : " + body.name());
    if(force.type() == "spring")
      draw_spring(force,world.selected_item() == "force_"+i);
    else if(force.type() == "linear_motor")
      draw_linear_motor(force,world.selected_item() == "force_"+i);
    else if(force.type() == "circular_motor")
      draw_circular_motor(force,world.selected_item() == "force_"+i);
      
    if(world.selected_item() == "force_"+i){
      var handles = handles_for_item(force);
      for(var j = 0; j < handles.length; j++){
        var h = handles[j];
        draw_handle(h.x,h.y);
      }
    }
  };
  for(var i = 0; i < world.joints().length; i++){
    var joint = world.joints()[i];
    //console.log("drawing body : " + body.name());
    if(joint.type() == "anchor")
      draw_anchor(joint,world.selected_item() == "joint_"+i);
    else if(joint.type() == "pin")
      draw_pin(joint,world.selected_item() == "joint_"+i);
    else if(joint.type() == "stick")
      draw_stick(joint,world.selected_item() == "joint_"+i);
  };
  for(var i = 0; i < world.misc().length; i++){
    var m = world.misc()[i];
    //console.log("drawing body : " + body.name());
    if(m.type() == "note")
      ;//notes are HTML divs inserted in the DOM, not drawn on canvas
    else if(m.type() == "tracer")
      draw_tracer(m,world.selected_item() == "misc_"+i);
    else if(m.type() == "plane")
      draw_plane(m,world.selected_item() == "misc_"+i);
    else if(m.type() == "meter")
      draw_meter(m,world.selected_item() == "misc_"+i);
    else if(m.type() == "graph")
      draw_graph(m,world.selected_item() == "misc_"+i);
    else if(m.type() == "controller")
      draw_controller(m,world.selected_item() == "misc_"+i);
  };
}

function draw_plane(plane,is_selected){
  var x = plane.position.x();
  var y = plane.position.y();
  var diag = Math.sqrt(canvas.width*canvas.width + canvas.height*canvas.height)*world.space_scale();
  var dist = Math.sqrt((x-world.center.x())*(x-world.center.x())+(y-world.center.y())*(y-world.center.y()));
  var w = diag + 2*dist;
  context.translate(x,y);
  context.rotate(plane.angle());
  context.fillStyle = plane.color();
  context.fillRect(w / -2, -w, w, w);
  context.rotate(-plane.angle());
  context.translate(-x,-y);
}

function draw_box(body,is_selected){
  var x = body.position.x();
  var y = body.position.y();
  
  context.translate(x,y);
  context.rotate(body.angle());
  //context.beginPath();
  //context.rect(x, y, width, height);
  context.fillStyle = body.color();
  //context.fill();
  context.fillRect(body.width() / -2, body.height() / -2, body.width(), body.height());

  context.beginPath();
  context.moveTo(0,0);
  context.lineTo(body.width()/2,0);
  context.strokeStyle = world.color();
  context.stroke();
  if(is_selected){
    context.beginPath();
    context.setLineDash([5*world.space_scale(),2*world.space_scale()]);
    var newwidth = body.width() + 10*world.space_scale();
    var newheight = body.height() + 10*world.space_scale();
    context.strokeStyle = body.color();
    context.strokeRect(newwidth / -2, newheight / -2, newwidth, newheight);
    context.setLineDash([]);
  }
  context.rotate(-body.angle());
  context.translate(-x, -y);
  //context.translate();
  // draw velocity and acceleration vector arrows
  //draw_arrow(body.position.x(), body.position.y(),body.position.x()+body.velocity.x(),body.position.y()+body.velocity.y(),"green");
  //draw_arrow(body.position.x(), body.position.y(),body.position.x()+body.acceleration.x(),body.position.y()+body.acceleration.y(),"red");
  draw_va_arrows(body.position.x(), body.position.y(), body.velocity, body.acceleration, 2*world.space_scale());
  
  //draw_handle(body.position.x()+body.velocity.x(), body.position.y()+body.velocity.y());
  //draw_handle(body.position.x()+body.width()*Math.cos(body.angle())/2, body.position.y()+body.width()*Math.sin(body.angle())/2);
}

function draw_meter(meter,is_selected){
  var x = meter.position.x();
  var y = meter.position.y();
  context.translate(x,y);
  context.strokeStyle = meter.color();
  //context.strokeRect(meter.width() * world.space_scale() / -2, meter.height() * world.space_scale() / -2, meter.width() * world.space_scale(), meter.height() * world.space_scale());
  context.font = 'normal ' + (10 * world.space_scale()) + 'pt Calibri';
  context.textBaseline = 'bottom';
  //var value = get_meter_value(meter.object(),meter.property());
  var value = '35.0 m/s';
  context.scale(1,-1);
  var metrics = context.measureText(value);
  context.fillText(value, metrics.width*0.1, 0.1*10*world.space_scale());
  context.scale(1,-1);
  //console.log("text width = " + metrics.width);
  context.strokeRect(0, 0, metrics.width*1.4, 10 * world.space_scale()*1.4);
  context.translate(-x,-y);
};

function draw_disk(body,is_selected){
  context.beginPath();
  context.moveTo(body.position.x(), body.position.y());
  context.arc(body.position.x(), body.position.y(), body.radius(), body.angle()+0.025,body.angle() + 2*Math.PI - 0.025, false);
  context.fillStyle = body.color();
  context.fill();
  
  if(is_selected){
    context.beginPath();
    context.setLineDash([5*world.space_scale(),2*world.space_scale()]);
    context.arc(body.position.x(), body.position.y(), body.radius() + 5 * world.space_scale(), 0, 2 * Math.PI, false);
    context.strokeStyle = body.color();
    context.stroke();
    context.setLineDash([]);
  }
  // draw velocity and acceleration vector arrows
  draw_va_arrows(body.position.x(), body.position.y(),body.velocity,body.acceleration,2*world.space_scale());
  //draw_arrow(body.position.x(), body.position.y(),body.position.x()+body.velocity.x(),body.position.y()+body.velocity.y(),"green");
  //draw_arrow(body.position.x(), body.position.y(),body.position.x()+body.acceleration.x(),body.position.y()+body.acceleration.y(),"red");
  //draw_handle(body.position.x()+body.velocity.x(), body.position.y()+body.velocity.y());
  //draw_handle(body.position.x()+body.radius()*Math.cos(body.angle()), body.position.y()+body.radius()*Math.sin(body.angle()));
}

function draw_handle(x,y){
  /*context.beginPath();
  context.arc(x, y, world.scene.theme.size.handle() * world.space_scale(), 0, 2 * Math.PI, false);
  context.strokeStyle = world.scene.theme.colors.handle();
  context.stroke(); */
  context.fillStyle = "black";
  context.fillRect(x - 4*world.space_scale(), y - 4*world.space_scale(), 8*world.space_scale(), 8*world.space_scale());
}

function draw_stick(stick, is_selected){
  var tmp1 = find_body_by_name(stick.body_1());
  var tmp2 = find_body_by_name(stick.body_2());
  var b1 = tmp1[0];
  var b2 = tmp2[0];
  var p1x = b1 ? b1.position.x() : stick.position_1.x();
  var p1y = b1 ? b1.position.y() : stick.position_1.y();
  var p2x = b2 ? b2.position.x() : stick.position_2.x();
  var p2y = b2 ? b2.position.y() : stick.position_2.y();

  var lx = p2x - p1x;
  var ly = p2y - p1y;
  //context.save();
  context.beginPath();
  context.moveTo(p1x,p1y);
  context.lineTo(p2x,p2y);
  context.lineWidth = 2 * world.space_scale();
  context.strokeStyle = stick.color();
  context.stroke();

/*  context.moveTo(spring.position_1.x(),spring.position_1.y());
  context.lineTo(spring.position_2.x(),spring.position_2.y());
  context.lineWidth = 2 * world.space_scale();
  context.strokeStyle = spring.color();
  context.stroke();
*/  
  /*context.beginPath();
  context.arc(p1x, p1y, (particle_radius / 2) * world.space_scale(), 0, 2 * Math.PI, false);
  context.fillStyle = stick.color();
  context.fill();

  context.beginPath();
  context.arc(p2x, p2y, (particle_radius / 2) * world.space_scale(), 0, 2 * Math.PI, false);
  context.fillStyle = stick.color();
  context.fill(); */
  //context.restore();
};

function context_original_transform(){
  context.setTransform(1,0,0,1,0,0);
  context.strokeStyle = "#ccc";
  context.lineWidth = 1;
};

function context_grid_transform(){
  //flip y-axis and move origin to center
  context.setTransform(1,0,0,-1,canvas.width/2,canvas.height/2);
  context.strokeStyle = "#ccc";
  context.lineWidth = 1;
};

function context_world_transform(){
  var f = world.space_scale();
  //context_grid_transform(); // reset any existing transforms
  context.setTransform(1.0/f,0,0,-1.0/f,canvas.width/2 - world.center.x()/f,canvas.height/2 + world.center.y()/f);
  context.strokeStyle = "blue";
  context.lineWidth = f;
  //console.log("switched to world transform");
};

function clear_world(){
  //console.log("clearing world");
  context_grid_transform();
  //console.log("switching to grid transform");
  context.clearRect(-canvas.width/2, -canvas.height/2, canvas.width, canvas.height);
  if(world.scene.show_crosshair())
    draw_crosshair();
  context_world_transform();
  /*if(world.scene.show_grid()) //commenting out for now because the grid looks ugly
    draw_grid(); */
}

function draw_grid(){
  var ctx = context;
  var width = canvas.width;
  var height = canvas.height;
  var f = world.space_scale();

  ctx.beginPath();
  ctx.moveTo(0, 0);
  ctx.lineTo(150 * f, 0); //x-axis
  ctx.moveTo(0, -150 * f);
  ctx.lineTo(0, 150 * f); //y
  ctx.lineWidth = 4 * f;
  //ctx.strokeStyle = 'red';
  ctx.stroke();

  ctx.beginPath();
  ctx.lineWidth = 1 * f;
  ctx.strokeStyle = 'green'
  /*for (var i = 0; i < 5; i++) {
      ctx.moveTo((i * 30) * 1E9, 0);
      ctx.lineTo((i * 30) * 1E9, 150 * 1E9)
      //ctx.stroke();
  }

  for (var i = 0; i < 5; i++) {
      ctx.moveTo(0, (i*30) * 1E9);
      ctx.lineTo(150 * 1E9, (i * 30) * 1E9)
      //ctx.stroke();
  } */
  ctx.stroke();

}

function draw_crosshair(){
  //console.log("drawing axes");
  // y-axis
  context_world_transform();
  draw_arrow(world.center.x(),world.center.y()-100*world.space_scale(),world.center.x(),world.center.y()+100*world.space_scale(),"#aaa");
  draw_arrow(world.center.x()-100*world.space_scale(),world.center.y(),world.center.x()+100*world.space_scale(),world.center.y(),"#aaa");

/*  context.beginPath();
  context.moveTo(0, -100);
  context.lineTo(0, 100);
  //context.strokeStyle = "#ccc";
  //context.lineWidth = 1;
  context.stroke();
  
  // x-axis
  context.beginPath();
  context.moveTo(-100, 0);
  context.lineTo(100, 0);
  //context.strokeStyle = "#ccc";
  //context.lineWidth = 1;
  context.stroke();*/
  context_grid_transform();
  context.scale(1,-1);
  var origin = "(" + world.center.x() + "," + world.center.y() + ")";
  context.font = 'normal 8pt Calibri';
  context.fillStyle = "black";
  context.fillText(origin,-30,15);
  context.fillText(String(world.center.x() + 100*world.space_scale()),90,-10);
  context.fillText(String(world.center.y() + 100*world.space_scale()),10,-90);
  context.scale(1,-1);
}

function draw_spring(spring, is_selected){
  context.save();
  context.translate(spring.position_1.x(),spring.position_1.y());
  var tmp1 = find_body_by_name(spring.body_1());
  var tmp2 = find_body_by_name(spring.body_2());
  var b1 = tmp1[0];
  var b2 = tmp2[0];
  var p1x = b1 ? b1.position.x() : spring.position_1.x();
  var p1y = b1 ? b1.position.y() : spring.position_1.y();
  var p2x = b2 ? b2.position.x() : spring.position_2.x();
  var p2y = b2 ? b2.position.y() : spring.position_2.y();

  var lx = p2x - p1x;
  var ly = p2y - p1y;
  var ang = Math.atan2(ly,lx);

  var length = Math.sqrt(lx*lx + ly*ly);
  context.rotate(ang);
  context.beginPath();
  context.moveTo(0,0);
  //var lx_sign = (lx < 0 ? -1 : 1);
  //var ly_sign = (ly < 0 ? -1 : 1);
  for(var i = 0; i < 10 * Math.PI; i += 0.2){
    var x = (i/10/Math.PI) * length;// * lx_sign;
    var y = 15*Math.sin(i) * world.space_scale();// * lx_sign;
    context.lineTo(x,y);
  };
  //context.lineTo(length,0);
  context.lineWidth = 2 * world.space_scale();
  context.strokeStyle = spring.color();
  context.stroke();
  context.restore();
/*  context.moveTo(spring.position_1.x(),spring.position_1.y());
  context.lineTo(spring.position_2.x(),spring.position_2.y());
  context.lineWidth = 2 * world.space_scale();
  context.strokeStyle = spring.color();
  context.stroke();
*/
  draw_handle(p1x,p1y);
  draw_handle(p2x,p2y);
/*  context.beginPath();
  context.arc(p1x, p1y, (particle_radius / 2) * world.space_scale(), 0, 2 * Math.PI, false);
  context.fillStyle = spring.color();
  context.fill();

  context.beginPath();
  context.arc(p2x, p2y, (particle_radius / 2) * world.space_scale(), 0, 2 * Math.PI, false);
  context.fillStyle = spring.color();
  context.fill(); */
};

function draw_linear_motor(motor, is_selected){
  context.beginPath();
  context.arc(motor.position.x(), motor.position.y(), world.scene.theme.size.linear_motor() * world.space_scale(), 0, 2 * Math.PI, false); // radius is not scaled with zoom level (space_scale)
  context.fillStyle = motor.color();
  context.fill();
  if(is_selected){
    context.beginPath();
    context.setLineDash([5*world.space_scale(),2*world.space_scale()]);
    context.arc(motor.position.x(), motor.position.y(), world.scene.theme.size.linear_motor() * world.space_scale(), 0, 2 * Math.PI, false);
    context.strokeStyle = motor.color();
    context.stroke();
    context.setLineDash([]);
  }
  draw_arrow(motor.position.x(), motor.position.y(),motor.position.x()+motor.force.x(),motor.position.y()+motor.force.y(),"white",4*world.space_scale());
  //draw_handle(motor.position.x()+motor.force.x(),motor.position.y()+motor.force.y());
};

function draw_circular_motor(motor, is_selected){
  context.beginPath();
  context.arc(motor.position.x(), motor.position.y(), world.scene.theme.size.circular_motor() * world.space_scale(), 0, 2 * Math.PI, false); // particle_radius is not scaled with zoom level (space_scale)
  context.fillStyle = motor.color();
  context.fill();
  context.beginPath();
  context.arc(motor.position.x(), motor.position.y(), (world.scene.theme.size.circular_motor() +4) * world.space_scale(), 0, 2 * Math.PI, false);
  context.lineWidth = 2 * world.space_scale();
  context.strokeStyle = motor.color();
  context.stroke();
  if(is_selected){
    context.beginPath();
    context.setLineDash([5*world.space_scale(),2*world.space_scale()]);
    context.arc(motor.position.x(), motor.position.y(), (world.scene.theme.size.circular_motor()+7) * world.space_scale(), 0, 2 * Math.PI, false);
    context.strokeStyle = motor.color();
    context.stroke();
    context.setLineDash([]);
  }  
};

function draw_va_arrows(x,y,v,a,thickness){
  draw_arrow(x, y,x+v.x(),y+v.y(),"white",thickness);
  draw_arrow(x, y,x+a.x(),y+a.y(),"yellow",thickness);
}

function draw_arrow(fromx,fromy,tox,toy,color, thickness){
  if(fromx == tox && fromy == toy)
    return;
  context.save();
  context.beginPath();
  context.moveTo(fromx,fromy);
  context.lineWidth = thickness;
  context.lineTo(tox,toy)
  context.strokeStyle = color;
  context.stroke();
  
  context.translate(tox,toy);
  context.rotate(Math.atan2(toy-fromy,tox-fromx));

  context.beginPath();
  context.moveTo(0,0);
  context.lineWidth = thickness;
  context.lineTo(-5*world.space_scale(),5*world.space_scale())
  //context.strokeStyle = color;
  context.stroke();
  context.beginPath();
  context.moveTo(0,0);
  context.lineWidth = thickness;
  context.lineTo(-5*world.space_scale(),-5*world.space_scale())
  //context.strokeStyle = color;
  context.stroke();
  context.restore();
};

function draw_particle(body, is_selected){
  context.beginPath();
  context.arc(body.position.x(), body.position.y(), world.scene.theme.size.particle() * world.space_scale(), 0, 2 * Math.PI, false); // particle_radius is not scaled with zoom level (space_scale)
  context.fillStyle = body.color();
  context.fill();
  if(is_selected){
    context.beginPath();
    context.setLineDash([5*world.space_scale(),2*world.space_scale()]);
    context.arc(body.position.x(), body.position.y(), (world.scene.theme.size.particle()+4) * world.space_scale(), 0, 2 * Math.PI, false);
    context.strokeStyle = body.color();
    context.stroke();
    context.setLineDash([]);
  }

  //draw_arrow(body.position.x(), body.position.y(),body.position.x()+body.velocity.x(),body.position.y()+body.velocity.y(),"green");
  //draw_arrow(body.position.x(), body.position.y(),body.position.x()+body.acceleration.x(),body.position.y()+body.acceleration.y(),"red");
  draw_va_arrows(body.position.x(), body.position.y(), body.velocity, body.acceleration, 2*world.space_scale());
  //draw_handle(body.position.x()+body.velocity.x(), body.position.y()+body.velocity.y());
}

function draw_tracer(tracer, is_selected){
  context.beginPath();
  context.arc(tracer.position.x(), tracer.position.y(), world.scene.theme.size.tracer() * world.space_scale(), 0, 2 * Math.PI, false); // particle_radius is not scaled with zoom level (space_scale)
  context.strokeStyle = tracer.color();
  context.stroke();
  
  if(is_selected){
    context.beginPath();
    context.setLineDash([5*world.space_scale(),2*world.space_scale()]);
    context.arc(tracer.position.x(), tracer.position.y(), (world.scene.theme.size.tracer()+4) * world.space_scale(), 0, 2 * Math.PI, false);
    context.strokeStyle = tracer.color();
    context.stroke();
    context.setLineDash([]);
  }
  if(tracer.points().length > 1){
    context.beginPath();
    context.moveTo(tracer.points()[0][0],tracer.points()[0][1]);
    for(var i = 1; i < tracer.points().length; i++)
      context.lineTo(tracer.points()[i][0],tracer.points()[i][1]);
    //context.strokeStyle = tracer.color();
    context.stroke();
  }
};

function draw_anchor(anchor, is_selected){
  context.beginPath();
  context.arc(anchor.position.x(), anchor.position.y(), world.scene.theme.size.anchor() * world.space_scale(), 0, 2 * Math.PI, false); // particle_radius is not scaled with zoom level (space_scale)
  context.strokeStyle = anchor.color();
  context.stroke();
  if(is_selected){
    context.beginPath();
    context.setLineDash([5*world.space_scale(),2*world.space_scale()]);
    context.arc(anchor.position.x(), anchor.position.y(), (world.scene.theme.size.anchor()+4) * world.space_scale(), 0, 2 * Math.PI, false);
    context.stroke();
    context.setLineDash([]);
  }
};

function draw_pin(pin, is_selected){
  context.beginPath();
  context.arc(pin.position.x(), pin.position.y(), world.scene.theme.size.pin() * world.space_scale(), 0, 2 * Math.PI, false); // particle_radius is not scaled with zoom level (space_scale)
  context.fillStyle = pin.color();
  context.fill();
  
  if(is_selected){
    context.beginPath();
    context.setLineDash([5*world.space_scale(),2*world.space_scale()]);
    context.arc(pin.position.x(), pin.position.y(), (world.scene.theme.size.pin()+4) * world.space_scale(), 0, 2 * Math.PI, false);
    context.stroke();
    context.setLineDash([]);
  }
};

function world_coords(mousePos){
  return {
    x: (world.center.x() + (mousePos.x - canvas.width/2)*world.space_scale()),
    y: (world.center.y() + (canvas.height/2 - mousePos.y)*world.space_scale())
  };
}

function handle_angle(b,wp){
  //console.log("changing angle");
  var xdiff = wp.x - b.position.x();
  var ydiff = wp.y - b.position.y();
  b.angle(Math.atan2(ydiff,xdiff));
}

function handle_velocity(b,wp){
  //console.log("changing velocity");
  var vx = wp.x - b.position.x();
  var vy = wp.y - b.position.y();
  //b.velocity.x(handle_drag_start.x + x_shift * world.space_scale());
  //b.velocity.y(handle_drag_start.y - y_shift * world.space_scale());
  b.velocity.x(vx);
  b.velocity.y(vy);
}

function handle_disk_size(b,wp){
  var rx = wp.x - b.position.x();
  var ry = wp.y - b.position.y();
  b.radius(Math.sqrt(rx*rx+ry*ry));
}

function handle_box_size(b,wp){
  var rx = wp.x - b.position.x();
  var ry = wp.y - b.position.y();
  var d = Math.sqrt(rx*rx+ry*ry);
  var ang = Math.atan2(ry,rx) - b.angle();
  b.width(Math.abs(2*d*Math.cos(ang)));
  b.height(Math.abs(2*d*Math.sin(ang)));
}

function handle_spring_1(f,wp){
  f.position_1.x(wp.x);
  f.position_1.y(wp.y);
  var item = get_clicked_item(wp);
  if(item == "world"){
  } else if(item[0] == "body"){
    f.body_1(item[1].name());
  }
}

function handle_spring_2(f,wp){
  f.position_2.x(wp.x);
  f.position_2.y(wp.y);
  var item = get_clicked_item(wp);
  if(item == "world"){
  } else if(item[0] == "body"){
    f.body_2(item[1].name());
  }
}

function handle_lm_force(f,wp){
  f.force.x(wp.x - f.position.x());
  f.force.y(wp.y - f.position.y());
}

function handles_for_item(item){
  var handles = [];
  var h;
  if(item.type() == 'particle'){
    h = {};
    h.name = "velocity";
    h.x = item.position.x()+item.velocity.x();
    h.y = item.position.y()+item.velocity.y();
    h.fn = handle_velocity;
    handles.push(h);
  } else if(item.type() == 'disk'){
    h = {};
    h.name = "size";
    h.x = item.position.x()-item.radius()*Math.sin(item.angle());
    h.y = item.position.y()+item.radius()*Math.cos(item.angle());
    h.fn = handle_disk_size;
    handles.push(h);
    
    h = {};
    h.name = "velocity";
    h.x = item.position.x()+item.velocity.x();
    h.y = item.position.y()+item.velocity.y();
    h.fn = handle_velocity;
    handles.push(h);

    h = {};
    h.name = "angle";
    h.x = item.position.x()+item.radius()*Math.cos(item.angle());
    h.y = item.position.y()+item.radius()*Math.sin(item.angle());
    h.fn = handle_angle;
    handles.push(h);
  } else if(item.type() == 'box'){
    h = {};
    h.name = "size";
    var d = Math.sqrt(item.width()*item.width() + item.height()*item.height())/2;
    var ang = item.angle() + Math.atan(item.height()/item.width());
    h.x = item.position.x()+d*Math.cos(ang);
    h.y = item.position.y()+d*Math.sin(ang);
    h.fn = handle_box_size;
    handles.push(h);

    h = {};
    h.name = "velocity";
    h.x = item.position.x()+item.velocity.x();
    h.y = item.position.y()+item.velocity.y();
    h.fn = handle_velocity;
    handles.push(h);

    h = {};
    h.name = "angle";
    h.x = item.position.x()+item.width()*Math.cos(item.angle())/2;
    h.y = item.position.y()+item.width()*Math.sin(item.angle())/2;
    h.fn = handle_angle;
    handles.push(h);
  } else if(item.type() == 'spring'){
    var tmp1 = find_body_by_name(item.body_1());
    var tmp2 = find_body_by_name(item.body_2());
    var b1 = tmp1[0];
    var b2 = tmp2[0];
    var p1x = b1 ? b1.position.x() : item.position_1.x();
    var p1y = b1 ? b1.position.y() : item.position_1.y();
    var p2x = b2 ? b2.position.x() : item.position_2.x();
    var p2y = b2 ? b2.position.y() : item.position_2.y();
    
    h = {};
    h.name = "body1";
    h.x = p1x;
    h.y = p1y;
    h.fn = handle_spring_1;
    handles.push(h);

    h = {};
    h.name = "body2";
    h.x = p2x;
    h.y = p2y;
    h.fn = handle_spring_2;
    handles.push(h);
  } else if(item.type() == 'linear_motor'){
    h = {};
    h.name = "force";
    h.x = item.position.x()+item.force.x();
    h.y = item.position.y()+item.force.y();
    h.fn = handle_lm_force;
    handles.push(h);
  }
  return handles;
}

function resizeCanvas() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  draw_world();
}

function canvas_click_handler(evt){
  var mousePos = getMousePos(canvas, evt);
  var wp = world_coords(mousePos);
  var obj = get_clicked_item(wp);
  var current_action = window.user_model.selected_action();
  //console.log("click detected on ->");
  //console.log(obj);
  
  if(obj != "world"){
    //world.selected_body(body_index);
    if(obj[0] == 'body'){
      world.selected_item("body_" + obj[2]);
    }
    if(obj[0] == 'force'){
      world.selected_item("force_" + obj[2]);
    }
    if(obj[0] == 'joint'){
      world.selected_item("joint_" + obj[2]);
    }
    if(obj[0] == 'misc'){
      world.selected_item("misc_" + obj[2]);
    }
  } else {
    world.selected_item(null);
    if(current_action == 'add_particle'){
      add_particle(wp);
    } else if(current_action == 'add_disk'){
      add_disk(wp);
    } else if(current_action == 'add_box'){
      add_box(wp);
    } else if(current_action == 'add_plane'){
      add_plane(wp,0);
    } else if(current_action == 'add_spring'){
      add_spring(wp);
    } else if(current_action == 'add_linear_motor'){
      add_linear_motor(wp);
    } else if(current_action == 'add_circular_motor'){
      add_circular_motor(wp);
    } else if(current_action == 'add_note'){
      add_note(mousePos.x,mousePos.y);
    } else if(current_action == 'add_tracer'){
      add_tracer(wp);
    } else if(current_action == 'add_meter'){
      add_meter(wp);
    } else if(current_action == 'add_anchor'){
      add_anchor(wp);
    } else if(current_action == 'add_pin'){
      add_pin(wp);
    } else if(current_action == 'add_stick'){
      add_stick(wp);
    };
  }
  toolbar_clicked('select');
};

function is_click_in_shape(wx,wy,shape){
  if(shape[0] == 'circle'){
    var d = {x: shape[1].center.x - wx, y: shape[1].center.y - wy};
    var r = Math.sqrt(d.x*d.x + d.y*d.y);
    return (r <= shape[1].radius);
  } else if(shape[0] == 'rect'){
    var d = {x: wx - shape[1].center.x, y: wy - shape[1].center.y};
    var r = Math.sqrt(d.x*d.x + d.y*d.y);
    var a = Math.atan2(d.y,d.x);
    var angle = shape[1].angle;
    var d2 = {x: r*Math.cos(a - angle), y: r*Math.sin(a - angle)};
    return (Math.abs(d2.x) <= shape[1].width/2 && Math.abs(d2.y) <= shape[1].height/2);
  };
};

function get_clicked_item(wp){
  var wx = wp.x;// + world.center.x(); // + (mousePos.x - (canvas.width/2)) * world.space_scale();
  var wy = wp.y;// + world.center.y(); // + ((canvas.height/2) - mousePos.y) * world.space_scale();
  for(var i = 0; i < world.bodies().length; i++){
    var body = world.bodies()[i];
    var handles = handles_for_item(body);
    for(var j = 0; j < handles.length; j++){
      var h = handles[j];
      var shape = ['circle',{center: {x: h.x, y: h.y}, radius: world.scene.theme.size.handle()*world.space_scale()}];
      if(is_click_in_shape(wx,wy,shape)){
        return ["handle",body,h];
      }
    }
    var shape = bounding_shape_for_item(body);
    if(shape && is_click_in_shape(wx,wy,shape)){
      return ["body",body,i];
    };
  };  
  for(var i = 0; i < world.forces().length; i++){
    var force = world.forces()[i];
    var handles = handles_for_item(force);
    for(var j = 0; j < handles.length; j++){
      var h = handles[j];
      var shape = ['circle',{center: {x: h.x, y: h.y}, radius: world.scene.theme.size.handle()*world.space_scale()}];
      if(is_click_in_shape(wx,wy,shape)){
        return ["handle",force,h];
      }
    }
    var shape = bounding_shape_for_item(force);
    if(shape && is_click_in_shape(wx,wy,shape)){
      return ["force",force,i];
    };
  };
  for(var i = 0; i < world.joints().length; i++){
    var joint = world.joints()[i];
    var shape = bounding_shape_for_item(joint);
    if(shape && is_click_in_shape(wx,wy,shape)){
      return ["joint",joint,i];
    };
  };
  for(var i = 0; i < world.misc().length; i++){
    var misc = world.misc()[i];
    var shape = bounding_shape_for_item(misc);
    if(shape && is_click_in_shape(wx,wy,shape)){
      return ["misc",misc,i];
    };
  };
  return "world";
}

function mousedown_handler(evt){
  mouse_drag_start = getMousePos(canvas, evt);
  drag_type = get_clicked_item(world_coords(mouse_drag_start));
  //console.log("drag_type == "); console.log(drag_type);
  if(drag_type == "world")
    world_drag_start = {x: world.center.x(), y: world.center.y()};
  else if(drag_type[0] == 'handle'){
    //if(drag_type[2].name == "velocity")
      //handle_drag_start = {x: drag_type[1].velocity.x(), y: drag_type[1].velocity.y()};
    //else if(drag_type[2].name == "angle")
      var x = 0; // do nothing;
    }
  else //body,force,joint or misc
    if(drag_type[1].type() == 'spring') 
      item_drag_start = {x1: drag_type[1].position_1.x(), y1: drag_type[1].position_1.y(), x2: drag_type[1].position_2.x(), y2: drag_type[1].position_2.y()};
    else
      item_drag_start = {x: drag_type[1].position.x(), y: drag_type[1].position.y()};
  //console.log("mouse drag starting");
  //console.log(mouse_drag_start);
	window.addEventListener("mousemove", mousemove_handler, false);
	canvas.removeEventListener("mousedown", mousedown_handler, false);
  window.addEventListener("mouseup", mouseup_handler, false);
  
	//code below prevents the mouse down from having an effect on the main browser window:
	if (evt.preventDefault) {
		evt.preventDefault();
	} //standard
	else if (evt.returnValue) {
		evt.returnValue = false;
	} //older IE
	return false;
};

function mousemove_handler(evt){
  var mousePos = getMousePos(canvas, evt);
  var wp = world_coords(mousePos);
  //console.log("mouse moved");
  //console.log(mousePos);
  var x_shift = mousePos.x - mouse_drag_start.x;
  var y_shift = mousePos.y - mouse_drag_start.y;

  if(drag_type == "world"){
    world.center.x(world_drag_start.x - x_shift * world.space_scale());
    world.center.y(world_drag_start.y + y_shift * world.space_scale());
  } else if(drag_type[0] == 'handle') {
    var item = drag_type[1];
    var handle = drag_type[2];
    //console.log("dragging handler.."); console.log(handle);
    handle.fn(item, wp);
  } else { //item is being dragged
    if(drag_type[1].type() == 'linear_motor'){
      var dropped_at = get_clicked_item(wp);
      drag_type[1].position.x(item_drag_start.x + x_shift * world.space_scale());
      drag_type[1].position.y(item_drag_start.y - y_shift * world.space_scale());

      if(dropped_at == 'world' || dropped_at[0] != 'body'){
        drag_type[1].body(undefined);
        //drag_type[1].local_position.x(0);
        //drag_type[1].local_position.y(0);
      } else { // dropped on body
        drag_type[1].body(dropped_at[1].name());
        //drag_type[1].local_position.x(0);
        //drag_type[1].local_position.y(0);
      }
    } else if(drag_type[1].type() == 'circular_motor'){
      var dropped_at = get_clicked_item(wp);
      if(dropped_at == 'world' || dropped_at[0] != 'body'){
        drag_type[1].position.x(item_drag_start.x + x_shift * world.space_scale());
        drag_type[1].position.y(item_drag_start.y - y_shift * world.space_scale());
        drag_type[1].body(undefined);
      } else {
        //torque is applied at center
        drag_type[1].position.x(dropped_at[1].position.x());
        drag_type[1].position.y(dropped_at[1].position.y());
        drag_type[1].body(dropped_at[1].name());
      }
    } else if(drag_type[1].type() == 'anchor'){
      var dropped_at = get_clicked_item(wp);
      if(dropped_at == 'world' || dropped_at[0] != 'body'){
        drag_type[1].position.x(item_drag_start.x + x_shift * world.space_scale());
        drag_type[1].position.y(item_drag_start.y - y_shift * world.space_scale());
        drag_type[1].body(undefined);
      } else {
        //torque is applied at center
        drag_type[1].position.x(dropped_at[1].position.x());
        drag_type[1].position.y(dropped_at[1].position.y());
        drag_type[1].body(dropped_at[1].name());
      }
    } else if(drag_type[1].type() == 'pin'){
      var dropped_at = get_clicked_item(wp);
      drag_type[1].position.x(item_drag_start.x + x_shift * world.space_scale());
      drag_type[1].position.y(item_drag_start.y - y_shift * world.space_scale());

      if(dropped_at == 'world' || dropped_at[0] != 'body'){
        drag_type[1].body(undefined);
        drag_type[1].local_position.x(0);
        drag_type[1].local_position.y(0);
      } else { // dropped on body
        drag_type[1].body(dropped_at[1].name());
        //console.log("wp.x = ");console.log(wp.x);
        var rx = wp.x - dropped_at[1].position.x();
        var ry = wp.y - dropped_at[1].position.y();
        var d = Math.sqrt(rx*rx + ry*ry);
        var ang = Math.atan2(ry,rx);
        drag_type[1].local_position.x(d*Math.cos(ang - dropped_at[1].angle())); //fixme
        drag_type[1].local_position.y(d*Math.sin(ang - dropped_at[1].angle())); //fixme
      }
    } else if(drag_type[1].type() == 'spring'){
      //springs don't have position. They have position_1 and position_2
      drag_type[1].body_1(undefined);
      drag_type[1].body_2(undefined);
      drag_type[1].position_1.x(item_drag_start.x1 + x_shift * world.space_scale());
      drag_type[1].position_1.y(item_drag_start.y1 - y_shift * world.space_scale());
      drag_type[1].position_2.x(item_drag_start.x2 + x_shift * world.space_scale());
      drag_type[1].position_2.y(item_drag_start.y2 - y_shift * world.space_scale());
    } else { //body
      drag_type[1].position.x(item_drag_start.x + x_shift * world.space_scale());
      drag_type[1].position.y(item_drag_start.y - y_shift * world.space_scale());
    }
  };
};

function mouseup_handler(evt){
  drag_type = undefined;
	canvas.addEventListener("mousedown", mousedown_handler, false);
	window.removeEventListener("mouseup", mouseup_handler, false);
  window.removeEventListener("mousemove", mousemove_handler, false);
};

